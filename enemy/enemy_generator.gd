extends Node2D

@export var EnemyScene: PackedScene
@onready var spawner_component: = $SpawnerComponent
@onready var enemy_spawn_timer: = $EnemySpawnTimer
@onready var spawn1: = $spawnLocations/spawn1
@onready var spawn2: = $spawnLocations/spawn2
@onready var spawn3: = $spawnLocations/spawn3
@onready var spawn4: = $spawnLocations/spawn4
@onready var spawn5: = $spawnLocations/spawn5
@onready var spawn6: = $spawnLocations/spawn6
@onready var spawn7: = $spawnLocations/spawn7
@onready var spawn8: = $spawnLocations/spawn8
@onready var spawn9: = $spawnLocations/spawn9
@onready var spawn10: = $spawnLocations/spawn10
@onready var spawn11: = $spawnLocations/spawn11
@onready var spawn12: = $spawnLocations/spawn12

var spawnLocations = []
var screen_width = ProjectSettings.get_setting("display/window/size/viewport_width")

func _ready():
	enemy_spawn_timer.timeout.connect(handle_spawn.bind(EnemyScene, enemy_spawn_timer))
	spawnLocations = [
		spawn1.global_position,
		spawn2.global_position,
		spawn3.global_position,
		spawn4.global_position,
		spawn5.global_position,
		spawn6.global_position,
		spawn7.global_position,
		spawn8.global_position,
		spawn9.global_position,
		spawn10.global_position,
		spawn11.global_position,
		spawn12.global_position
	]

func handle_spawn(enemy_scene: PackedScene, timer: Timer) -> void:
	var location = spawnLocations.pick_random()
	spawner_component.scene = enemy_scene
	spawner_component.spawn(location, get_tree().root.get_node("Node2D/World"))
	timer.start()

