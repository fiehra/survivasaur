extends CharacterBody2D

@export var speed = 50
@export var nav_agent: NavigationAgent2D
@export var health_amount: int = 3
@export var damage_amount: int = 1

@onready var animations = $AnimationPlayer
@onready var score_component = $ScoreComponent
@onready var spawner_component: = $SpawnerComponent

var target_node = null
var lastXAxis = "Right"
var home_position: Vector2
#@export var limit = 0.5
var isDead = false
var initalizedHomePosition = false


func _ready():
	nav_agent.path_desired_distance = 4.0
	nav_agent.target_desired_distance = 4.0

func _physics_process(_delta):
	updateAnimation()
	move()

func move():
	if nav_agent.is_navigation_finished() || isDead:
		velocity = Vector2.ZERO
		return
	var axis = to_local(nav_agent.get_next_path_position()).normalized()
	if axis.x < 0:
		lastXAxis = "Left"
	else:
		lastXAxis = "Right"
		
	var intended_velocity = axis * speed
	nav_agent.set_velocity(intended_velocity)

	if target_node:
		if target_node.position.distance_to(position) < 10:
			velocity = Vector2.ZERO
		else:
			move_and_slide()	
	else:
		move_and_slide()
	
#func changeDirection():
	#var tempEnd = endPosition
	#endPosition = startPosition
	#startPosition = tempEnd

#func patrol():
	#var moveDirection = (endPosition - position)
	#if moveDirection.length() < limit:
		#changeDirection() 
	#velocity = moveDirection.normalized() * speed	
	
func recalc_path():
	if target_node:
		nav_agent.target_position = target_node.global_position
	else:
		if initalizedHomePosition:
			nav_agent.target_position = home_position
		else:
			home_position = global_position
			initalizedHomePosition = true
			nav_agent.target_position = home_position
			

func _on_calc_path_timer_timeout():
	recalc_path()

func _on_aggro_range_area_entered(area):
	target_node = area.owner

func _on_de_aggro_range_area_exited(area):
	if area.owner == target_node:
		target_node = null

func updateAnimation():
	if isDead:
		animations.play("damage")
	else:	
		if velocity.x > 0:
			animations.play("walkRight")
		elif velocity.x < 0: 
			animations.play("walkLeft")
		elif velocity.y > 0:
			if lastXAxis == "Right":
				animations.play("walkRight")
			elif lastXAxis == "Left":
				animations.play("walkLeft")
		elif velocity.y < 0:
			if lastXAxis == "Right":
				animations.play("walkRight")
			elif lastXAxis == "Left":
				animations.play("walkLeft")
		elif velocity.x == 0 && velocity.y == 0: 
			if lastXAxis == "Right":
				animations.play("idleRight")
			elif lastXAxis == "Left":
				animations.play("idleLeft")

func _on_hurtbox_area_entered(area:Area2D):
	isDead = true
	health_amount -= area.owner.damage
	if health_amount <= 0:
		score_component.adjust_score(1)
		spawner_component.spawn()
		queue_free()
	
func _on_hurtbox_area_exited(_area):
	await get_tree().create_timer(0.6).timeout
	isDead = false

func _on_navigation_agent_2d_velocity_computed(safe_velocity:Vector2):
	velocity = safe_velocity
