extends Node2D

@export var pickup_amount: int = 1


func _on_collect_area_area_entered(area:Area2D):
	if area.owner.name == "Player":
		HealthManager.increase_health(pickup_amount)
		queue_free()
