## survivasaur

## Description

2d survival game where you play a dinosaur. you must collect hearts to heal and kill enemies.<br>
you can use abilities like walk, dash and kick<br>

## Visuals

game screenshots: <br>
![start](/art/Screenshot%20from%202024-01-29%2022-13-44.png?raw=true "start")
![game](/art/Screenshot%20from%202024-01-29%2022-13-49.png?raw=true "game")

## Roadmap

my goal is to improve my game development skills.<br>
things i still want to add in this game are:<br>

- advanved pathfinding<br>
- collectibles<br>
- simple combat system<br>
- heals<br>
- ui and menu<br>
- multiple levels<br>

## License

open source you can use this as a a learning resource
