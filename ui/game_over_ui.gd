extends Control

const SAVE_PATH = "user://savegame.cfg"
@export var game_stats: GameStats

var save_path = SAVE_PATH

@onready var score_value: Label = $CenterContainer/VBoxContainer/HBoxContainer/ScoreValue
@onready var highscore_value: Label = $CenterContainer/VBoxContainer/HBoxContainer2/HighScoreValue

func _ready():
	loadScore()
	if game_stats.score > game_stats.highscore:
		game_stats.highscore = game_stats.score
		saveScore()
	score_value.text = str(game_stats.score)
	highscore_value.text = str(game_stats.highscore)

func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("ui_accept"):
		get_tree().change_scene_to_file("res://levels/world/world.tscn")
		resetGameStats()
	elif Input.is_action_just_pressed("ui_cancel"):
		get_tree().change_scene_to_file("res://ui/menu_ui.tscn")
		resetGameStats()

func resetGameStats():
	game_stats.score = 0
	HealthManager.current_health = 3

func loadScore() -> void:
	var config = ConfigFile.new()
	var error = config.load(save_path)
	if error != OK: return
	game_stats.highscore = config.get_value("game", "highscore")

func saveScore():
	var config = ConfigFile.new()
	config.set_value("game", "highscore", game_stats.highscore)
	config.save(save_path)