extends Node2D

@export var heart1: Texture2D
@export var heart0: Texture2D
@export var game_stats: GameStats

@onready var hp_1 = $hp1
@onready var hp_2 = $hp2
@onready var hp_3 = $hp3
@onready var score_label: Label = $ScoreLabel
@onready var info_label: Label = $InfoLabel

func _ready():
	update_score_label(game_stats.score)
	game_stats.score_changed.connect(update_score_label)
	on_player_health_changed(HealthManager.current_health)
	HealthManager.on_health_changed.connect(on_player_health_changed)

func on_player_health_changed(player_current_health: int):
	if player_current_health == 3:
		hp_1.texture = heart1
		hp_2.texture = heart1
		hp_3.texture = heart1
	elif player_current_health == 2:
		hp_1.texture = heart1
		hp_2.texture = heart1
		hp_3.texture = heart0
	elif player_current_health == 1:
		hp_1.texture = heart1
		hp_2.texture = heart0
		hp_3.texture = heart0
	elif player_current_health == 0:
		hp_1.texture = heart0
		hp_2.texture = heart0
		hp_3.texture = heart0

func update_score_label(new_score: int) -> void:
	score_label.text = str(new_score)

func _on_hide_info_timer_timeout():
	$InfoLabel.queue_free()
