extends CharacterBody2D

var blast = preload("res://player/blast.tscn")
var blastLeft = preload("res://player/blastleft.tscn")
@onready var kickBlast: Marker2D = $kickblast
var blastPosition

@export var speed: int = 66
@export var friction = 1200
@onready var axis = Vector2.ZERO
var dashActive = false
var lastXAxis = "Right"
@onready var animations = $AnimationPlayer
var kick = false

func _ready():
	blastPosition = kickBlast.position

func _physics_process(delta):
	kick_blast_position()
	useKick()
	updateAnimation()
	move(delta)

func get_input_axis():
	axis = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	axis.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	axis.y = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
	return axis.normalized()
	
func move(delta):
	if kick:
		return
	else:
		axis = get_input_axis()
		if axis == Vector2.ZERO:
			apply_friction(friction * delta)
		else:
			if Input.is_action_pressed("dash"):
				dashActive = true
				velocity = axis * speed * 1.5
			else: 
				dashActive = false
				velocity = axis * speed
		if axis.x > 0:
			lastXAxis = "Right"
		elif axis.x < 0:
			lastXAxis = "Left"
		else:
			lastXAxis = lastXAxis
		move_and_slide()

func apply_friction(amount):
	if velocity.length() > amount:
		velocity -= velocity.normalized() * amount
	else:
		velocity = Vector2.ZERO

func updateAnimation():
	if kick:
		if lastXAxis == "Right":
			animations.play("kick")
		elif lastXAxis == "Left":
			animations.play("kickLeft")
	else:
		if velocity.x > 0:
			if dashActive:
				animations.play("dashRight")
			else: 
				animations.play("walkRight")
		elif velocity.x < 0: 
			if dashActive:
				animations.play("dashLeft")
			else: 
				animations.play("walkLeft")
		elif velocity.y > 0:
			if lastXAxis == "Right":
				if dashActive:
					animations.play("dashRight")
				else:
					animations.play("walkRight")
			elif lastXAxis == "Left":
				if dashActive:
					animations.play("dashLeft")
				else: 
					animations.play("walkLeft")
		elif velocity.y < 0:
			if lastXAxis == "Right":
				if dashActive:
					animations.play("dashRight")
				else: 
					animations.play("walkRight")
			elif lastXAxis == "Left":
				if dashActive:
					animations.play("dashLeft")
				else: 
					animations.play("walkLeft")
		elif velocity.x == 0 && velocity.y == 0: 
			if lastXAxis == "Right":
				animations.play("idleRight")
			elif lastXAxis == "Left":
				animations.play("idleLeft")

func useKick():
	if kick:
		return
	else:
		if Input.is_action_just_pressed("kick"):
			print_debug("Kick")
			kick = true
			var blastInstance
			if lastXAxis == "Right":
				blastInstance = blast.instantiate() as Node2D
				blastInstance.global_position = kickBlast.global_position
			else:
				blastInstance = blastLeft.instantiate() as Node2D
				blastInstance.global_position = kickBlast.global_position
			get_parent().add_child(blastInstance)
			await get_tree().create_timer(0.6).timeout
			kick = false
			
func kick_blast_position():
	if lastXAxis == "Right":
		kickBlast.position.x = blastPosition.x
	else:
		kickBlast.position.x = -blastPosition.x
		
func _on_hurtbox_area_entered(area:Area2D):
	HealthManager.decrease_health(area.owner.damage_amount)
	if HealthManager.current_health <= 0:
		await get_tree().create_timer(0.6).timeout
		get_tree().change_scene_to_file("res://ui/game_over_ui.tscn")
